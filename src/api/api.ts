import { Order } from "./order";

interface ApiCallParams {
  method: "get" | "post" | "put" | "delete";
  body?: any;
  query?: string;
}

async function apiCall<T>({ method, body, query }: ApiCallParams): Promise<T | null> {
  const response = await fetch(`https://fkcsiiogj2.execute-api.us-east-1.amazonaws.com/default/AcademyEx?${query}`, {
    method,
    body: body && JSON.stringify(body),
  });
  switch (response.status) {
    case 200:
      return response.json();
    default:
      return null;
  }
}
/*
export const postOrder = (name: string, address: string, type: string) =>
  apiCall<number>({
    method: "post",
    body: { name, address, type },
  });

export const getOrders = () =>
  apiCall<Array<Order>>({
    method: "get",
  });
*/
