type CoffeeType = "Cappuccino" | "Latte" | "Short Black" | "Espresso";

export interface Order {
  id: number;
  customerName: string;
  deliveryAddress: string;
  type: CoffeeType;
}
