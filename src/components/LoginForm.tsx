import { Box, Button, TextField, Typography } from "@mui/material";
import { useFormik } from "formik";
import React from "react";
import * as yup from "yup";

const schema = yup.object({
  userName: yup.string().oneOf(["admin"], "Incorrect username or password").required("Incorrect username or password"),
  password: yup.string().oneOf(["admin"], "Incorrect username or password").required("Incorrect username or password"),
});

export const LoginForm = () => {
  const formik = useFormik({
    initialValues: {
      userName: "",
      password: "",
    },
    validationSchema: schema,
    onSubmit: () => {
      window.location.href = "/admin";
    },
  });
  return (
    <Box>
      <Typography>Admin Login</Typography>
      <form onSubmit={formik.handleSubmit}>
        <Box sx={{ display: "flex", flexDirection: "column", width: "30vw", "& > div": { margin: "10px 0" } }}>
          <TextField
            label="Username"
            value={formik.values.userName}
            onChange={(event) => formik.setFieldValue("userName", event.target.value)}
          />
          <TextField
            label="Password"
            type="password"
            value={formik.values.password}
            onChange={(event) => formik.setFieldValue("password", event.target.value)}
          />
          {formik.touched.password &&
            formik.touched.userName &&
            (!!formik.errors.password || !!formik.errors.userName) && (
              <Typography color="red">{formik.errors.password || formik.errors.userName}</Typography>
            )}

          <Button type="submit">Login</Button>
        </Box>
      </form>
    </Box>
  );
};
