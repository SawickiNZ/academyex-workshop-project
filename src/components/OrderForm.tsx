import { Autocomplete, Box, Button, TextField, Typography } from "@mui/material";
import { LoadingButton } from "@mui/lab";
import { useFormik } from "formik";
import React, { useState } from "react";
import * as yup from "yup";

const schema = yup.object({
  name: yup.string().required("The above field is required"),
  address: yup.string().required("The above field is required"),
  type: yup.string().required("The above field is required"),
});

export const OrderForm = () => {
  const [loading, setLoading] = useState(false);
  const [submitted, setSubmitted] = useState(false);
  const formik = useFormik({
    initialValues: {
      name: "",
      address: "",
      type: "",
    },
    validationSchema: schema,
    onSubmit: () => {
      setLoading(true);
      window.setTimeout(() => {
        setLoading(false);
        setSubmitted(true);
      }, 1000);
    },
  });
  return (
    <Box>
      <Typography>Order a Coffee</Typography>
      <form onSubmit={formik.handleSubmit}>
        <Box sx={{ display: "flex", flexDirection: "column", width: "30vw", "& > div": { margin: "10px 0" } }}>
          <TextField
            label="Name"
            value={formik.values.name}
            onChange={(event) => formik.setFieldValue("name", event.target.value)}
            error={formik.touched.name && !!formik.errors.name}
            helperText={formik.touched.name && formik.errors.name}
          />
          <TextField
            label="Delivery Address"
            value={formik.values.address}
            onChange={(event) => formik.setFieldValue("address", event.target.value)}
            error={formik.touched.address && !!formik.errors.address}
            helperText={formik.touched.address && formik.errors.address}
          />

          <Autocomplete
            value={formik.values.type}
            renderInput={(props) => (
              <TextField
                label="Type"
                {...props}
                error={formik.touched.type && !!formik.errors.type}
                helperText={formik.touched.type && formik.errors.type}
              ></TextField>
            )}
            onChange={(event, value) => formik.setFieldValue("type", value)}
            options={["Espresso", "Cappuccino", "Latte", "Short Black"]}
          />
          {submitted ? (
            <Button disabled variant="contained" color="success" sx={{ backgroundColor: "green" }}>
              Order Submitted!
            </Button>
          ) : (
            <LoadingButton type="submit" loading={loading}>
              Place Order
            </LoadingButton>
          )}
        </Box>
      </form>
    </Box>
  );
};
