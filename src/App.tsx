/*
Demo website - demonstration of cyber attack techniques
TODO - 
Broken access control - Potentially have like a button on the mainpage that lets you login as admin
  or something which then causes a username/password prompt in order to navigate to the admin site.
  The exploit is that we show that if you look at the sources on developers console, you can figure 
  out that you can just normally navigate to it.
  - Another one could be that once logged in, a user can access any other user's data (eg, if there 
    is a userId parameter in the url, and user ids are just sequential integers, it is very easy to 
    guess another user's id, as opposed to if GUIDs were used).

identification & authentication failures - For this was thinking maybe have a hardcoded list of 
  usernames and passwords and allow users to prompt to know if the website has a particular 
  username - if it exists it will be something like "incorrect password" if it does then
  "Incorrect username and password". We also like include common usernames such as "admin" and
  make a few accounts with common passwords such as "1234", "princess", "password", etc. 

Insecure design - Still thinking about this one but like the example #2 on OWASP, maybe include
  some small shop/form/input and make a vulnerability in it where people can spam/ddos/overload 
  the service showing that the design was insecure leading to some loss/embarassment 

*/

import React, { ReactElement } from "react";
import "./App.css";
import { LoginPage } from "./pages/LoginPage";
import { AdminPage } from "./pages/AdminPage";

// Think global probs might not be needed as there are no like AJAX requests/fetch from DB.

/*BUTTON HANDLER - Assuming we just make a handler to save the values of username/password
  once the button is pressed (event) and then match the user/pwd combo to the harcoded array. Depending
  on outcome we display a text under the form giving away info. Upon correct password naviagte to the 
  admin site.
*/

/*INSECURE DESIGN - We flexbox 2 divs to have one side with the login and the other with the 
  improvised shop/booking system something along the lines. Make a custom TSX element for this 
  and allow it to be broken in a similar way to the OWASP specifications.
*/

/*Make a login as admin button which nagivates to the admin page - you are then prompted with a 
  javascript/typescript prompt() function asking for the username and password in order to access
  the site. In order to see the admin website you should be able to simply modify the input within
  the developer tools in order to see the admin website and essentially capture the flag.
*/
const pageElement = {
  login: <LoginPage />,
  admin: <AdminPage />,
} as Record<string, ReactElement>;

function App() {
  const page = window.location.pathname.split("/")[1].split("?")[0] || "login";
  return (
    <div className="app">
      <header className="app-header">
        <h1>The Coffee Shop</h1>
      </header>
      {pageElement[page]}
      <footer className="app-footer">
        <p>© {new Date().getFullYear()} Jakub, Natalya & Aneela</p>
      </footer>
    </div>
  );
}

export default App;
