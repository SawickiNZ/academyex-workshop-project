import React, { useEffect, useState } from "react";
import { Order } from "../api/order";
import { Box, TableBody, TableCell, TableHead, TableRow } from "@mui/material";

export const AdminPage = () => {
  const orderList = [
    { customerName: "Joe", deliveryAddress: "123 Mockingbird Lane,Springfield, Anytown 12345", type: "Cappuccino" },
    { customerName: "Lilly", deliveryAddress: "456 Elm Street, Willowville, Imaginaria 56789", type: "Espresso" },
    { customerName: "Alex", deliveryAddress: "789 Pine Avenue, Maplewood, Faketown 67890", type: "Latte" },
    { customerName: "Liam", deliveryAddress: "1010 Oak Drive, Cedarville, Dreamland 54321", type: "Cappuccino" },
    { customerName: "Mia", deliveryAddress: "1111 Sunset Boulevard, Beachville, Fantasyland 98765", type: "Espresso" },
    { customerName: "John", deliveryAddress: "1313 River Road, Brooksville, Wonderland 24680", type: "Latte" },
    { customerName: "Richard", deliveryAddress: "1515 Mountain View, Hillside, Neverland 13579", type: "Latte" },
    { customerName: "Fiona", deliveryAddress: "1717 Meadow Lane, Bloomville, Makebelieve 86420", type: "Espresso" },
  ] as Order[];

  return (
    <Box>
      <TableBody>
        <TableHead>
          <TableCell>Customer Name</TableCell>
          <TableCell>Delivery Address</TableCell>
          <TableCell>Coffee Type</TableCell>
        </TableHead>
        {orderList?.map((o) => (
          <TableRow>
            <TableCell>{o.customerName}</TableCell>
            <TableCell>{o.deliveryAddress}</TableCell>
            <TableCell>{o.type}</TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Box>
  );
};
