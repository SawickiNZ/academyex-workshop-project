import React from "react";
import { Box, Divider } from "@mui/material";
import { LoginForm } from "../components/LoginForm";
import { OrderForm } from "../components/OrderForm";

export const LoginPage = () => {
  return (
    <Box sx={{ display: "flex", justifyContent: "space-evenly", padding: "40px" }}>
      <LoginForm />
      <Divider orientation="vertical" flexItem />
      <OrderForm />
    </Box>
  );
};
